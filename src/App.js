import React, { Component } from 'react';
import './App.css';
import HomePage from './components/Home/homePage';
import {Switch, Route, Redirect} from 'react-router-dom'
import Shop from './components/shop/Shop';
import Header from './components/header/Header';
import SignInSignUp from './components/sign-in-and-sign-up/SignInSignUp';
import { auth, createUserProfileDocument } from './firebase/firebase.util';
import { connect } from 'react-redux';
import { setCurrentUser } from './redux/user/user.actions';
import { createStructuredSelector } from 'reselect'
import { selectCurrentUser } from './redux/user/user.selectors';
import Checkout from './components/checkout/Checkout';

class App extends Component {
  
  unsubscribeFromAuth = null;

  componentDidMount(){
    const {setCurrentUser} = this.props;

    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth);

        userRef.onSnapshot(snapShot => {
          setCurrentUser({            
              id:snapShot.id,
              ...snapShot.data()
          })
        })
      }
      setCurrentUser(userAuth);
      
    })
  }
  componentWillUnmount(){
    this.unsubscribeFromAuth()
  }
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path='/' render={()=> this.props.currentUser ? (<HomePage  />) : (<SignInSignUp />)}></Route>
          <Route exact path='/signin'  render={()=> this.props.currentUser ? (<Redirect to='/' />) : (<SignInSignUp />)}></Route>
          <Route path='/shop' component={Shop}></Route>
          <Route exact path='/checkout' component={Checkout}></Route>
        </Switch>
      </div>
    )
  }
}
const mapStateToProps = createStructuredSelector({
  currentUser : selectCurrentUser
})
const mapDispatchToProps = dispatch => ({
  setCurrentUser : user => dispatch(setCurrentUser(user))
})
export default connect(mapStateToProps,mapDispatchToProps)(App)