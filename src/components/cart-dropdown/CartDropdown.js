import React from 'react'
import './CartDropdown.scss'
import CustomButton from '../custom-button/CustomButton'
import { createStructuredSelector } from 'reselect'
import CartItem from '../cart-items/CartItem'
import { connect } from 'react-redux'
import { selectCartItemsCount, selectCartItems } from '../../redux/cart/cart.selectors'
import { withRouter } from 'react-router-dom'
import { toggleCartHidden } from '../../redux/cart/cart.actions'

const CartDropdown = ({cartItems, history, dispatch}) => {
    return (

    <div className='cart-dropdown'>
        <div className='cart-items'>
            {
                cartItems.length ? (
                cartItems.map(cartItem => 
                (<CartItem key={cartItem.key} item={cartItem}></CartItem>
                )))
                : (
                <span className='empty-message'>Your cart is empty</span>
                )
            }
        </div>
        <CustomButton onClick={() =>{
            history.push('/checkout');
            dispatch(toggleCartHidden());
            }}
        >Go To Checkout</CustomButton>
    </div>
)}
const mapStateToProps = createStructuredSelector ({
    cartItems : selectCartItems
})

export default withRouter(connect(mapStateToProps )(CartDropdown))
