import React from 'react'
import './SignInSignUp.scss'
import SignIn from './SignIn'
import SignUp from '../sign-up/SignUp'
function SignInSignUp() {
    return (
        <div className='sign-in-and-sign-up'>
        <SignIn></SignIn>
        <SignUp></SignUp>
        </div>
    )
}

export default SignInSignUp
